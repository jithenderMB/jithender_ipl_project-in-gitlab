const fs = require('fs');
const path = require('path');

const csvToJson = require('csvtojson');
const matchesCsvFilePath = path.resolve(__dirname, '../data/matches.csv');
const deliveriesCsvFilePath = path.resolve(__dirname, '../data/deliveries.csv');

const ipl = require('./ipl');


const writeFile = (path, data) => {
    fs.writeFileSync(path, data, 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    });
};

csvToJson()
    .fromFile(matchesCsvFilePath)
    .then((matchesJsonObj) => {
        //Number of matches played per year for all the years in IPL.
        let iplTotalMatchesPerYear = ipl.matchesPerYear(matchesJsonObj);
        writeFile(path.resolve(__dirname, '../public/output/matchesPerYear.json'),
            JSON.stringify(iplTotalMatchesPerYear))

        //Number of matches won per team per year in IPL.
        let matchesTeamWonPerYear = ipl.matchesWonByTeamsPerYear(matchesJsonObj);
        writeFile(path.resolve(__dirname, '../public/output/matchesTeamWonPerYear.json'),
            JSON.stringify(matchesTeamWonPerYear))

        //Get all match Ids in the given year 
        let matchIdsInYear2016 = ipl.getMatchIdsOfSpecificYear(matchesJsonObj, '2016');
        let matchIdsInYear2015 = ipl.getMatchIdsOfSpecificYear(matchesJsonObj, '2015');

        // get all match ids per season
        let allMatchIdsPerYear = ipl.getMatchIdsWithSeason(matchesJsonObj);

        // Find the number of times each team won the toss and also won the match
        let teamsWonTossAndMatch = ipl.getTeamsWonTossAndMatch(matchesJsonObj);
        writeFile(path.resolve(__dirname, '../public/output/teamsWonTossAndMatch.json'),
            JSON.stringify(teamsWonTossAndMatch))

        // Find a player who has won the highest number of Player of the Match awards for each season
        let highestManOfMatchPerSeason = ipl.getPlayerWithHighestManOfMatchPerSeason(matchesJsonObj);
        writeFile(path.resolve(__dirname, '../public/output/highestManOfMatchPerSeason.json'),
            JSON.stringify(highestManOfMatchPerSeason))

        csvToJson()
            .fromFile(deliveriesCsvFilePath)
            .then((deliveriesJsonObj) => {
                //Extra runs conceded per team in the year 2016
                let extraRunsOfteamsIn2016 = ipl.getExtraRunsForEachTeamPerYear(deliveriesJsonObj, matchIdsInYear2016);
                writeFile(path.resolve(__dirname, '../public/output/extraRunsOfTeamsInYear2016.json'),
                    JSON.stringify(extraRunsOfteamsIn2016))

                // Top 10 economical bowlers in the year 2015
                let top10BowlersEconomyIn2015 = ipl.getTop10BowlersEconomy(deliveriesJsonObj, matchIdsInYear2015);
                writeFile(path.resolve(__dirname, '../public/output/top10BowlersEconomyInYear2015.json'),
                    JSON.stringify(top10BowlersEconomyIn2015))

                // Find the strike rate of a batsman for each season
                let strikeRateOfPlayersBySeason = ipl.getStrikeRateOfPlayersBySeason(deliveriesJsonObj, allMatchIdsPerYear);
                writeFile(path.resolve(__dirname, '../public/output/strikeRateOfPlayersBySeason.json'),
                    JSON.stringify(strikeRateOfPlayersBySeason))

                // Find the highest number of times one player has been dismissed by another player
                let highestDismissalsOfBatsmanByBowler = ipl.getHighestDismissalsOfBatsmanByBowler(deliveriesJsonObj, allMatchIdsPerYear);
                writeFile(path.resolve(__dirname, '../public/output/highestDismissalsOfBatsmanByBowler.json'),
                    JSON.stringify(highestDismissalsOfBatsmanByBowler))

                // Find the bowler with the best economy in super overs
                let bestPlayerEconomyInSuperOver = ipl.getBestPlayerEconomyInSuperOvers(deliveriesJsonObj, allMatchIdsPerYear);
                writeFile(path.resolve(__dirname, '../public/output/bestPlayerEconomyInSuperOver.json'),
                    JSON.stringify(bestPlayerEconomyInSuperOver))
            });
    });





