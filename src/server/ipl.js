//Number of matches played per year for all the years in IPL.
const matchesPerYear = (userArray) => {
    const result = userArray.reduce((acc, cur) => { //acc - Accumulator & cur - Current value
        if (acc[cur.season]) {
            acc[cur.season] = ++acc[cur.season];
        }
        else {
            acc[cur.season] = 1;
        }
        return acc;
    }, {})
    return result;
}

//Number of matches won per team per year in IPL.
const matchesWonByTeamsPerYear = (userArray) => {
    let result = userArray.reduce((acc, cur) => {
        let year = cur.season;
        let winner = cur.winner;
        if (acc[year]) {
            if (acc[year][winner]) {
                acc[year][winner] += 1;
            }
            else {
                if (winner != '') {
                    acc[year][winner] = 1;
                }
            }
        }
        else {
            acc[year] = {}
            acc[year][winner] = 1;
        }
        return acc
    }, {});
    return result;
}

//Get all match Ids in the given year 

const getMatchIdsOfSpecificYear = (matchDetails, year) => {

    const result = matchDetails
        .reduce((acc, curr) => {
            let { id, season } = curr;
            if (acc[id] && id != '' && season === year) {
                acc[id] = season;
            }
            else if (season === year) {
                acc[id] = season;
            }
            return acc;
        }, {})
    return result;
}

//Extra runs conceded per team in the year 2016

const getExtraRunsForEachTeamPerYear = (deliveriesDetail, matchIds) => {

    const result = deliveriesDetail
        .reduce((acc, cur) => {

            let { match_id, bowling_team, extra_runs } = cur;
            let extraRuns = Number(extra_runs);

            if (acc[bowling_team] && matchIds[match_id] === '2016') {

                acc[bowling_team] += extraRuns;
            }
            else if (matchIds[match_id] === '2016') {

                acc[bowling_team] = extraRuns;
            }
            return acc
        }, {})

    return result
};


// Top 10 economical bowlers in the year 2015


const getTop10BowlersEconomy = (userArray, matchIds) => {
    let playerEconomyObj = {};
    const calculateEconomyRate = userArray
        .reduce((acc, cur) => {

            let { match_id, bowler, total_runs } = cur;
            let runsPerBall = parseInt(total_runs);

            if (acc[bowler] && matchIds[match_id] === '2015') {

                acc[bowler].runs += runsPerBall;
                acc[bowler].balls += 1;

                let runsTotal = acc[bowler].runs;
                let ballsTotal = acc[bowler].balls;
                let currentEconomy = (runsTotal / (ballsTotal / 6)).toFixed(2);

                acc[bowler].economy = Number(currentEconomy);
                playerEconomyObj[bowler] = Number(currentEconomy);
            }
            else if (matchIds[match_id] === '2015') {

                acc[cur.bowler] = { runs: runsPerBall, balls: 1, economy: 0 };
            }
            return acc;

        }, {})

    let result = {};
    let sortedEconomyRate = Object.values(playerEconomyObj).sort((a, b) => { return a - b });

    for (let economyRate of sortedEconomyRate.slice(0, 10)) {

        for (let [player, playerDetails] of Object.entries(calculateEconomyRate)) {

            if (economyRate === playerDetails.economy) {

                result[player] = { 'economy': economyRate };
            }
        }
    }

    return result;
}

// Find the number of times each team won the toss and also won the match
const getTeamsWonTossAndMatch = (userArray) => {

    let data = {};

    for (let element of userArray) {

        let tossWin = element.toss_winner;
        let matchWinner = element.winner;

        if (tossWin === matchWinner) {

            if (!data[matchWinner]) {
                data[matchWinner] = 1;
            }
            else {
                data[matchWinner] = ++data[matchWinner];
            }
        }
    }
    return data;
}

// Find a player who has won the highest number of Player of the Match awards for each season
const getPlayerWithHighestManOfMatchPerSeason = (userArray) => {

    let manOfMatchPerSeason = userArray.reduce((acc, curr) => {

        let { season, player_of_match } = curr;

        if (acc[season]) {
            if (acc[season][player_of_match]) {
                acc[season][player_of_match] += 1;
            }
            else {
                if (player_of_match != '') {
                    acc[season][player_of_match] = 1;
                }
            }
        }
        else {
            acc[season] = {}
            acc[season][player_of_match] = 1;
        }

        return acc

    }, {});

    let result = {};

    for (let element of Object.keys(manOfMatchPerSeason)) {

        let highetWins = Math.max(...Object.values(manOfMatchPerSeason[element]));

        for (let [player, manOfMatchCount] of Object.entries(manOfMatchPerSeason[element])) {

            if (manOfMatchCount === highetWins) {

                if (result[element]) {
                    result[element][player] = manOfMatchCount;
                } else {
                    result[element] = {};
                    result[element][player] = manOfMatchCount;
                }
            }

        }
    }
    return result;
}

// get all match ids per season
const getMatchIdsWithSeason = (matchDetails) => {

    const result = matchDetails
        .reduce((acc, curr) => {
            let { id, season } = curr;
            if (acc[id] && id != '') {
                acc[id] = season;
            }
            else {
                acc[id] = season;
            }
            return acc;
        }, {})

    return result;

}

// Find the strike rate of a batsman for each season
const getStrikeRateOfPlayersBySeason = (userArray, matchIds) => {

    const calculateEconomyRate = userArray
        .reduce((acc, curr) => {

            let { match_id, batsman, total_runs } = curr;
            let season = matchIds[match_id];

            let runsPerBall = parseInt(total_runs);

            if (acc[season]) {

                if (acc[season][batsman]) {

                    acc[season][batsman].runs += runsPerBall;
                    acc[season][batsman].balls += 1;

                    let runsTotal = acc[season][batsman].runs;
                    let ballsTotal = acc[season][batsman].balls;
                    let CurrentStrikeRate = (runsTotal / ballsTotal) * 100

                    acc[season][batsman].strikeRate = Number(CurrentStrikeRate.toFixed(2));
                }
                else if (batsman != '') {

                    acc[season][batsman] = { runs: runsPerBall, balls: 1, strikeRate: 0 };
                }

            }
            else {

                acc[season] = {};
                acc[season][batsman] = { runs: runsPerBall, balls: 1, strikeRate: 0 };
            }
            return acc;

        }, {})

    for (let [season, playersDetails] of Object.entries(calculateEconomyRate)) {

        for (let [player, details] of Object.entries(playersDetails)) {

            calculateEconomyRate[season][player] = details.strikeRate;
        }
    }

    return calculateEconomyRate;
};

// Find the highest number of times one player has been dismissed by another player
const getHighestDismissalsOfBatsmanByBowler = (userArray) => {

    let maxDimissal = 0;

    let result = userArray.reduce((acc, curr) => {

        let { bowler, player_dismissed } = curr;

        if (acc[player_dismissed]) {

            if (acc[player_dismissed][bowler]) {

                acc[player_dismissed][bowler] += 1;

                if (acc[player_dismissed][bowler] > maxDimissal) {

                    maxDimissal = acc[player_dismissed][bowler];
                }
            }
            else {
                if (player_dismissed != '') {
                    acc[player_dismissed][bowler] = 1;
                }
            }
        }
        else if (player_dismissed != '') {

            acc[player_dismissed] = {}
            acc[player_dismissed][bowler] = 1;
        }

        return acc

    }, {})

    let highestobj = {};

    for (let [playerDismissed, bowlers] of Object.entries(result)) {

        let batsman = playerDismissed;

        for (let [bowler, numberOfDismissel] of Object.entries(bowlers)) {

            if (numberOfDismissel === maxDimissal) {

                let string = `${batsman} is dismissed by ${bowler}`;

                highestobj[string] = maxDimissal;
            }
        }
    }

    return highestobj;


};


// Find the bowler with the best economy in super overs
const getBestPlayerEconomyInSuperOvers = (userArray) => {

    let playerEconomyArray = {};

    const calculateEconomyRate = userArray
        .reduce((acc, curr) => {

            let { bowler, is_super_over, total_runs } = curr;

            let superOver = parseInt(is_super_over);
            let runsPerBall = parseInt(total_runs);

            if (acc[bowler]) {

                if (superOver === 1) {

                    acc[bowler].runs += runsPerBall;
                    acc[bowler].balls += 1;

                    let runsTotal = acc[bowler].runs;
                    let ballsTotal = acc[bowler].balls;
                    let currentEconomy = Number((runsTotal / (ballsTotal / 6)).toFixed(2));

                    acc[bowler].economy = currentEconomy;
                    playerEconomyArray[bowler] = currentEconomy;
                }

            }
            else if (superOver === 1 && bowler != '') {

                acc[bowler] = { runs: runsPerBall, balls: 1, economy: 0 };
            }
            return acc;

        }, {})

    let result = {};
    let bestEconomy = Math.min(...Object.values(playerEconomyArray));

    for (let [player, deatils] of Object.entries(calculateEconomyRate)) {

        if (deatils.economy === bestEconomy) {

            result[player] = deatils.economy;
        }
    }
    return result;

}

module.exports = {
    matchesPerYear,
    matchesWonByTeamsPerYear,
    getMatchIdsOfSpecificYear,
    getExtraRunsForEachTeamPerYear,
    getTop10BowlersEconomy,
    getTeamsWonTossAndMatch,
    getPlayerWithHighestManOfMatchPerSeason,
    getMatchIdsWithSeason,
    getStrikeRateOfPlayersBySeason,
    getHighestDismissalsOfBatsmanByBowler,
    getBestPlayerEconomyInSuperOvers
};